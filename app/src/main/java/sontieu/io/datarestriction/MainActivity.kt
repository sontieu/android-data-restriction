package sontieu.io.datarestriction

import android.content.Intent
import android.net.TrafficStats
import android.os.Bundle
import android.support.v4.content.LocalBroadcastManager
import android.support.v7.app.AppCompatActivity
import android.util.Log
import android.widget.CheckBox
import android.widget.EditText
import eu.chainfire.libsuperuser.Shell
import kotlinx.android.synthetic.main.activity_main.*;

class MainActivity : AppCompatActivity() {
    private val TAG = MainActivity::class.java.simpleName

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        val pref = getSharedPreferences(Constants.PREF, MODE_PRIVATE)



        btnStart.setOnClickListener {
            val totalTraffic = (TrafficStats.getTotalRxBytes() + TrafficStats.getTotalTxBytes()) / 1024

            if (edtLimit.text.toString() != "")
                pref.edit()
                        .putLong(Constants.LIMIT, java.lang.Long.parseLong(edtLimit.text.toString()))
                        .apply()

            if (cbReset.isChecked)
                pref.edit()
                        .putLong(Constants.ORI, totalTraffic)
                        .apply()

            if (edtInterval.text.toString() != "")
                pref.edit()
                        .putLong(Constants.INTERVAL, java.lang.Long.parseLong(edtInterval.text.toString()))
                        .apply()

            if (!DataRestrictionService.isInstanceCreated)
                startService(Intent(this@MainActivity, DataRestrictionService::class.java))
        }

        btnStop.setOnClickListener { LocalBroadcastManager.getInstance(this@MainActivity).sendBroadcast(Intent(Constants.BR_STOP_TRACKING)) }
    }


    private fun requestSU() {
        if (Shell.SU.available()) {
            Log.d(TAG, "SU Version: " + Shell.SU.version(false))
        }
    }
}
