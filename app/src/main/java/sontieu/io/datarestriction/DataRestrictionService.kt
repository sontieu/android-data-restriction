package sontieu.io.datarestriction

import android.app.Notification
import android.app.NotificationManager
import android.app.PendingIntent
import android.app.Service
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.content.SharedPreferences
import android.graphics.BitmapFactory
import android.media.RingtoneManager
import android.net.ConnectivityManager
import android.net.TrafficStats
import android.os.Build
import android.os.Handler
import android.os.IBinder
import android.provider.Settings
import android.support.annotation.IntDef
import android.support.v4.app.NotificationCompat
import android.support.v4.content.ContextCompat
import android.support.v4.content.LocalBroadcastManager
import android.telephony.TelephonyManager
import android.util.Log
import android.widget.RemoteViews
import android.widget.Toast

import java.lang.reflect.Method
import java.util.Date

import eu.chainfire.libsuperuser.Shell

class DataRestrictionService : Service() {
    private var handler: Handler? = null
    private var r: Runnable? = null

    override fun onBind(intent: Intent): IBinder? {
        // TODO: Return the communication channel to the service.
        throw UnsupportedOperationException("Not yet implemented")
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        instance = this
        handler = Handler()

        val cm = getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
        val notiMng = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        val receiver = object : BroadcastReceiver() {
            override fun onReceive(context: Context, intent: Intent) {
                Log.d(TAG, "Request disable Data Restriction Service")
                handler!!.removeCallbacks(r)
                instance = null
                notiMng.cancel(2255)
                stopSelf()
            }
        }
        val intentFilter = IntentFilter(Constants.BR_STOP_TRACKING)
        LocalBroadcastManager.getInstance(this).registerReceiver(receiver, intentFilter)

        r = object : Runnable {
            override fun run() {
                val pref = getSharedPreferences(Constants.PREF, Context.MODE_PRIVATE)
                val limit = pref.getLong(Constants.LIMIT, 0)
                val ori = pref.getLong(Constants.ORI, 0)
                val interval = pref.getLong(Constants.INTERVAL, 0) * 1000

                val totalMb = (TrafficStats.getTotalRxBytes() + TrafficStats.getTotalTxBytes()) / 1024
                val usage = totalMb - ori
                Log.d(TAG, "Ori: $ori , current: $totalMb")

                val intentMain = Intent(this@DataRestrictionService, MainActivity::class.java)
                intentMain.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
                val pendingMain = PendingIntent.getActivity(this@DataRestrictionService, 8890, intentMain, PendingIntent.FLAG_UPDATE_CURRENT)

                if (usage > limit * 1024) {
                    // turn off mobile data
                    try {
                        val c = Class.forName(cm.javaClass.name)
                        val m = c.getDeclaredMethod("getMobileDataEnabled")
                        m.isAccessible = true
                        val isEnable = m.invoke(cm) as Boolean

                        if (isEnable) {
                            Shell.SU.run("svc data disable")

                            val noti = NotificationCompat.Builder(this@DataRestrictionService)
                                    .setAutoCancel(false)
                                    .setSmallIcon(R.mipmap.ic_launcher_round)
                                    .setContentTitle("Auto disconnect Mobile Data")
                                    .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                                    .setContentIntent(pendingMain)
                                    .build()
                            notiMng.notify(2256, noti)
                        }

                    } catch (e: Exception) {
                        e.printStackTrace()
                    }

                }


                val noti = NotificationCompat.Builder(this@DataRestrictionService)
                        .setAutoCancel(false)
                        .setSmallIcon(R.mipmap.ic_launcher_round)
                        .setContentTitle("Usage: " + usage / 1024 + " MB / " + limit + " MB")
                        .setContentIntent(pendingMain)
                        .build()
                notiMng.notify(2255, noti)

                handler!!.postDelayed(this, interval)
            }
        }
        handler!!.post(r)



        return Service.START_STICKY
    }

    override fun onDestroy() {
        super.onDestroy()

        instance = null
    }

    companion object {
        private val TAG = DataRestrictionService::class.java.simpleName
        private var instance: DataRestrictionService? = null


        val isInstanceCreated: Boolean
            get() = instance != null
    }
}
