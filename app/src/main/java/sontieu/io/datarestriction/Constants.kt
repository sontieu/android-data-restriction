package sontieu.io.datarestriction

/**
 * Created by ttson
 * Date: 6/10/2017.
 */

object Constants {
    val PREF = "pref"
    val LIMIT = "limit"
    val ORI = "data.originnal"
    val INTERVAL = "interval"

    val BR_STOP_TRACKING = "data.stop.tracking"
}
